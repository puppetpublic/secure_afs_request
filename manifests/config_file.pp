class secure_afs_request::config_file (
  Enum['present', 'absent'] $ensure         = 'present',
  Boolean                   $testmode       = true,
  Boolean                   $verbose        = false,
  Optional[String]          $test_address   = undef,
  String                    $db_config_file = '/etc/secure-afs-request/database.ini',
  String                    $from_address   = 'afs-request@stanford.edu',
  Hash[String, String]      $remctl_servers = {},
) {

  # Make sure the configuration directory exists (or not).
  case $ensure {
    'present': {
      file { '/etc/secure-afs-request':
        ensure => directory,
      }
    }
    'absent': {
      file { '/etc/secure-afs-request':
        ensure => absent,
      }
    }
  }

  # Define the remctl endpoints (needed by config.yaml.erb).
  $remctl_server_afs_info   = $remctl_servers['afs-info']
  $remctl_server_afs_manage = $remctl_servers['afs-manage']
  $remctl_server_wg_int     = $remctl_servers['wg-int']

  # Install the configuration file
  file { '/etc/secure-afs-request/config.yaml':
    ensure  => $ensure,
    content => template('secure_afs_request/etc/secure-afs-request/config.yaml.erb'),
    require => File['/etc/secure-afs-request'],
  }


}
