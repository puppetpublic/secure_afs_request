#
# Suggested name for wallet object containing the database credentials:
# db/<group>/secure-afs-request/<dbname>
#
class secure_afs_request (
  Enum['present', 'absent'] $ensure = 'present',
  #
  Enum['traditional', 'docker']
                          $hosting_model = 'traditional',
  #
  String                  $vhost        = 'secure-afs-request.example.com',
  String                  $port         = '443',
  String                  $server_admin = 'bogus@bogus.bogus',
  Boolean                 $ssl_enable   = true,
  #
  Enum['wallet', 'none']  $db_credentials_source = 'wallet',
  Optional[String]        $db_wallet_name        = undef,
  #
  Optional[String]        $ldap_wallet_name      = undef,
) {

  # Install the software.
  package {
    'secure-afs-request-web': ensure => $ensure,
  }

  class { 'secure_afs_request::config_file':
    ensure  => $ensure,
    require => Package['secure-afs-request-web'],
  }

  # Install the Apache configuration. If this is a traditional server we
  # save it as /etc/apache2/sites-enabled/secure-afs-request.conf. If this
  # is a docker container we put it in
  # /etc/apache2/sites-enabled/default.conf
  if ($hosting_model == 'traditional') {
    $apache_conf_name = '/etc/apache2/sites-enabled/secure-afs-request.conf'
  } else {
    $apache_conf_name = '/etc/apache2/sites-enabled/default.conf'
  }

  file { $apache_conf_name:
    ensure  => $ensure,
    content => template('secure_afs_request/etc/apache2/sites-available/secure-afs-request.conf.erb'),
    owner   => 'root',
    group   => 'root',
    mode    => '0755',
  }

  # We need the mod_include Apache module. Restart Apache only if
  # we are in the traditional hosting model.
  case $hosting_model {
    'traditional': { $restart_apache_flag = true  }
    'docker':      { $restart_apache_flag = false }
    default:       { $restart_apache_flag = true  }
  }
  su_apache::module { 'include':
    ensure         => present,
    restart_apache => $restart_apache_flag,
  }

  # Install the database ini file and other secrets (traditional hosts only)
  if ($hosting_model == 'traditional') {
    case $db_credentials_source {
      'wallet': {
        if ($db_wallet_name != undef) {
          wallet { $db_wallet_name:
            ensure => $ensure,
            type   => 'file',
            path   => '/etc/secure-afs-request/database.ini',
            mode   => '0640',
            owner  => 'root',
            group  => 'www-data',
          }
        } else {
          crit('wallet_name not defined')
        }
      }
      'none': { }
      default: { crit('unknown value for db_credentials_source') }
    }

    # Install the key-pair
    su_apache::cert::incommon { $vhost:
      ensure => $ensure,
    }

    # We need a Kerberos keytab for LDAP lookups.
    wallet { $ldap_wallet_name:
      ensure => $ensure,
      type   => 'keytab',
      path   => '/etc/secure-afs-request/keytab',
      mode   => '0640',
      owner  => 'root',
      group  => 'www-data',
    }
  }

}
